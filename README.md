# The Dialect client-server API spec

## Current version (0.5.0)
### [Rendered](https://app.swaggerhub.com/apis-docs/CabinetCapacity/Dialect/0.5.0)
### [OpenAPI Spec](openapi-spec/0.5.0.yaml)
